﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPG : Weapon
{

    public GameObject kaboomText;

    void Start()
    {
        am = GameObject.Find("_GM").GetComponent<AudioManager>();
        weaponName = "RPG";
        damage = 400f;
        magSize = 1;
        bulletsPerShot = 1;
        isReloading = false;
        rateOfFire = 5f;
        bulletsInMag = magSize;
        ammoOfMagazineLeft = 4;
        isFullAuto = false;
        effectiveRange = 5000;
    }

    public override void Shoot()
    {
        am.Play("Explosion");
        GameObject kapow = Instantiate(kaboomText, Vector3.zero, transform.rotation);
        Destroy(kapow, 3f);
    }
}
