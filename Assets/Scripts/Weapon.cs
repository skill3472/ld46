﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public AudioManager am;
    public string weaponName;
    public float damage;
    public int magSize;
    public int bulletsInMag;
    public int ammoOfMagazineLeft;
    public int bulletsPerShot;
    public bool isReloading;
    public bool isFullAuto;
    public float rateOfFire;
    public float effectiveRange;
    public float lastShot;

    public abstract void Shoot();

    void Update()
    {
        if(isFullAuto && Input.GetButton("Fire1") && Time.time > rateOfFire + lastShot && bulletsInMag > 0)
        {
            bulletsInMag -= bulletsPerShot;
            Shoot();
            lastShot = Time.time;
        }
        if(!isFullAuto && Input.GetButtonDown("Fire1") && Time.time > rateOfFire + lastShot && bulletsInMag > 0)
        {
            bulletsInMag -= bulletsPerShot;
            Shoot();
            lastShot = Time.time;
        }
        if(Input.GetButtonDown("Reload") && ammoOfMagazineLeft >= magSize)
        {
            am.Play("Reload");
            ammoOfMagazineLeft -= magSize;
            bulletsInMag = magSize;
        }
        else if(Input.GetButtonDown("Reload") && ammoOfMagazineLeft > 0 && magSize > ammoOfMagazineLeft)
        {
            am.Play("Reload");
            bulletsInMag = ammoOfMagazineLeft;
            ammoOfMagazineLeft = 0;
        }
    }
}
