﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodGenerator : MonoBehaviour
{

    public Sprite[] bloods;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = bloods[Random.Range(0, bloods.Length)];
        Destroy(gameObject, 15f);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
