﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    [SerializeField]private bool waveInProgress;
    [SerializeField]private float lastCheck;
    public int currentWave;
    [Header("Spawning Ratio")]
    public float growthRate;
    public float bonusEnemies;
    public float spawnDelay;
    [Space]
    [Header("Spawn Points")]
    public float spawnY;
    public float spawnXmin;
    public float spawnXmax;
    [Space]
    [Header("Enemies")]
    public GameObject[] enemies;
    [Space]
    [Header("Debug")]
    public int starterWave;
    public GameObject newWaveButton;



    // Start is called before the first frame update
    void Start()
    {
        SpawnWave(starterWave);
        currentWave = starterWave;
        newWaveButton.SetActive(false);
        waveInProgress = true;
    }

    // Update is called once per frame
    void Update()
    {
        lastCheck += Time.deltaTime;
        if(lastCheck >= 1f)
        {
            if(GameObject.FindGameObjectWithTag("Enemy") == null && waveInProgress)
            {
                WaveEnd();
            }
            else
            {
                lastCheck = 0f;
            }
        }
    }

    void SpawnWave(int wave)
    {
        StartCoroutine("SpawningItself", wave);
    }
    void WaveEnd()
    {
        Debug.Log("Wave Ended!");
        newWaveButton.SetActive(true);
        waveInProgress = false;
    }

    IEnumerator SpawningItself(int waveNumber)
    {
        for (int i = 0; i < Mathf.RoundToInt(Mathf.Pow(growthRate, waveNumber) + bonusEnemies); i++) {
            Debug.Log("Enemy number " + i.ToString() + " spawned.");
            Instantiate(enemies[(int)Mathf.Ceil(Random.Range(0, enemies.Length))], new Vector3(Random.Range(spawnXmin, spawnXmax), spawnY, 0f), Quaternion.identity);
            yield return new WaitForSeconds(spawnDelay);
        }
    }

    public void NextWave()
    {
        currentWave++;
        SpawnWave(currentWave);
        newWaveButton.SetActive(false);
        waveInProgress = true;
    }

}
