﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GM : MonoBehaviour
{

    public int money;
    public int score;
    public int life;

    public TextMeshProUGUI wavesText;
    public TextMeshProUGUI moneyText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI lifeText;
    public WaveSpawner spawner;

    // Start is called before the first frame update
    void Start()
    {
        money = 0;
        score = 0;
        life = 5;
        GetComponent<AudioManager>().Play("Clown Laugh");
        if(spawner == null)
        {
            Debug.Log("Failed to find a wave spawner, trying to assign one now...");
            spawner = gameObject.GetComponent<WaveSpawner>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        lifeCheck();
        UIUpdate();
    }

    public void OnQuitButton()
    {
        SceneManager.LoadScene(0);
    }

    public void looseLife(int lifesToLoose)
    {
        life -= lifesToLoose;
        GetComponent<AudioManager>().Play("Clown Hit");
    }

    void lifeCheck()
    {
        if(life <= 0)
        {
            Defeat();
        }
    }

    void Defeat()
    {
        Debug.Log("Pennywise got hurt. You monster.");
        //You failed screen, ect. goes here
    }

    public void GetPrizes(int moneyToGet, int scoreToGet)
    {
        money += moneyToGet;
        score += scoreToGet;
    }

    void UIUpdate()
    {
        wavesText.SetText("{0}", spawner.currentWave);
        moneyText.SetText("${0}", money);
        scoreText.SetText("{0}", score);
        lifeText.SetText("{0}", life);
    }
}
