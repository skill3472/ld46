﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{

    public Transform currentWeapon;
    public int selectedWeaponNo;
    [SerializeField] private Camera cam;
    public Vector3 mousePos;
    public Vector3 perpendicular;
    public Image currentWeaponImage;

    // Start is called before the first frame update
    void Start()
    {
        if(cam == null)
        {
            Debug.Log("No camera found, trying to assign one...");
            GameObject.Find("Main Camera");
        }

        SelectWeapon();
    }

    // Update is called once per frame
    void Update()
    {

        LookTowardsMouse();
        WeaponSwitching();
        currentWeaponImage.sprite = currentWeapon.gameObject.GetComponent<SpriteRenderer>().sprite;

    }

    void LookTowardsMouse()
    {
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

        perpendicular = Vector3.Cross(currentWeapon.position-mousePos,Vector3.forward);
        currentWeapon.rotation = Quaternion.LookRotation(Vector3.forward, perpendicular);
    }

    void SelectWeapon()
    {
        int i = 0;
        foreach (Transform weapon in transform)
        {
            if(i == selectedWeaponNo)
            {
                weapon.gameObject.SetActive(true);
                currentWeapon = weapon;
            } else
            {
                weapon.gameObject.SetActive(false);
            }
            i++;
        }
    }

    void WeaponSwitching()
    {
        int previousSelectedWeapon = selectedWeaponNo;
        if(Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (selectedWeaponNo >= transform.childCount - 1)
            {
                selectedWeaponNo = 0;
            } else
            {
                selectedWeaponNo++;
            }
        }
        if(Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (selectedWeaponNo <= 0)
            {
                selectedWeaponNo = transform.childCount - 1;
            } else
            {
                selectedWeaponNo--;
            }
        }
        if(previousSelectedWeapon != selectedWeaponNo)
        {
            SelectWeapon();
        }
    }
}
