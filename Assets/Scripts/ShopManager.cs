﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopManager : MonoBehaviour
{

    public GM gm;
    public bool isAmmo = false;
    [SerializeField]private GameObject ammoTab;
    [SerializeField]private GameObject weaponsTab;

    [SerializeField]private GameObject assaultRifleItem;
    [SerializeField]private GameObject sniperRifleItem;
    [SerializeField]private GameObject RPGItem;

    private bool isAssaultBought = false;
    private bool isSniperBought = false;
    private bool isRPGBought = false;

    public Transform player;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ammoButton()
    {
        isAmmo = true;
        UpdateShop();
    }

    public void weaponsButton()
    {
        isAmmo = false;
        UpdateShop();
    }

    void UpdateShop()
    {
        if(isAmmo)
        {
            weaponsTab.SetActive(false);
            ammoTab.SetActive(true);
        }
        else
        {
            weaponsTab.SetActive(true);
            ammoTab.SetActive(false);
        }
    }

    void Buy(int price)
    {
        if(gm.money >= price)
        {
            Debug.Log("Bought an item for " + price.ToString());
            GameObject.Find("_GM").GetComponent<AudioManager>().Play("Shop Sound");
        } else
        {
            Debug.Log("You can't afford that! You need " + (price - gm.money).ToString() + " more dollars!");
        }
    }

    public void buyAssault()
    {
        if(!isAssaultBought)
        {
            Buy(1337); //For debugging purposes, change it later
            GameObject assault = Instantiate(assaultRifleItem, transform.position, transform.rotation);
            assault.transform.parent = player;
            assault.transform.localPosition = Vector3.zero;
        }
    }
    public void buySniper()
    {
        if(!isSniperBought)
        {
            Buy(4200); //For debugging purposes, change it later
            GameObject snipey = Instantiate(sniperRifleItem, transform.position, transform.rotation);
            snipey.transform.parent = player;
            snipey.transform.localPosition = Vector3.zero;
        }
    }
    public void buyRPG()
    {
        if(!isRPGBought)
        {
            Buy(6900); //For debugging purposes, change it later
            GameObject kaboom = Instantiate(RPGItem, transform.position, transform.rotation);
            kaboom.transform.parent = player;
            kaboom.transform.localPosition = Vector3.zero;
        }
    }

    public void buyPistolAmmo()
    {
        Buy(500);
        player.gameObject.GetComponent<PlayerManager>().currentWeapon.gameObject.GetComponent<Pistol>().ammoOfMagazineLeft +=  player.gameObject.GetComponent<PlayerManager>().currentWeapon.gameObject.GetComponent<Pistol>().magSize * 7;
    }

    public void buyAssaultAmmo()
    {
        if(isAssaultBought)
        {
            Buy(500);
            player.gameObject.GetComponent<PlayerManager>().currentWeapon.gameObject.GetComponent<AssaultRifle>().ammoOfMagazineLeft +=  player.gameObject.GetComponent<PlayerManager>().currentWeapon.gameObject.GetComponent<AssaultRifle>().magSize * 7;
        }
    }

    public void buySniperAmmo()
    {
        if(isSniperBought)
        {
            Buy(500);
            player.gameObject.GetComponent<PlayerManager>().currentWeapon.gameObject.GetComponent<SniperRifle>().ammoOfMagazineLeft +=  player.gameObject.GetComponent<PlayerManager>().currentWeapon.gameObject.GetComponent<SniperRifle>().magSize * 7;
        }
    }

    public void buyRPGAmmo()
    {
        if(isRPGBought)
        {
            Buy(0);
            player.gameObject.GetComponent<PlayerManager>().currentWeapon.gameObject.GetComponent<RPG>().ammoOfMagazineLeft++;
        }
    }
}
