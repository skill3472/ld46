﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperRifle : Weapon
{
    void Start()
    {
        am = GameObject.Find("_GM").GetComponent<AudioManager>();
        weaponName = "Sniper Rifle";
        damage = 150f;
        magSize = 7;
        bulletsPerShot = 1;
        isReloading = false;
        rateOfFire = 2f;
        bulletsInMag = magSize;
        ammoOfMagazineLeft = magSize * 7;
        isFullAuto = false;
        effectiveRange = 10000;
    }

    public override void Shoot()
    {
        am.Play("Sniper Rifle");
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.right, effectiveRange);
        if(hitInfo.collider != null) //Trafilo w cos
        {
            Debug.Log("Trafilo w cos");
            //Debug.DrawLine(transform.position, hitInfo.point, Color.blue, 0.5f);
            if(hitInfo.collider.tag == "Enemy")
            {
                Debug.Log("Hit an enemy with " + damage.ToString() + " damage!");
                hitInfo.collider.GetComponent<Enemy>().DealDamage(damage);
            }
        } else //Nie trafilo w nic
        {
            Debug.Log("Nie trafilo w nic!");
            //Debug.DrawLine(transform.position, transform.position + transform.right * effectiveRange, Color.red, 0.5f);
        }


    }
}
