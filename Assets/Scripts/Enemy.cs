﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [Header("Enemy Stats")]
    public float health;
    public float movementSpeed;
    [Space]
    [Header("Blood")]
    [SerializeField]private GameObject blood;
    [Space]
    [Header("Textures")]
    [SerializeField]private Sprite[] textures;

    // Start is called before the first frame update
    void Start()
    {
        int character = Random.Range(0,2);
        if(character == 0) {GameObject.Find("_GM").GetComponent<AudioManager>().Play("Hater Female");}
        else if(character == 1) {GameObject.Find("_GM").GetComponent<AudioManager>().Play("Hater Male 1");}
        else if(character == 2) {GameObject.Find("_GM").GetComponent<AudioManager>().Play("Hater Male 2");}
        gameObject.GetComponent<SpriteRenderer>().sprite = textures[character];
    }

    // Update is called once per frame
    void Update()
    {
         transform.position += -transform.up * Time.deltaTime * movementSpeed;
    }

    public void DealDamage(float damageDealt)
    {
        health -= damageDealt;
        if(health <= 0)
        {
            Debug.Log("The enemy has been killed!");
            GameObject.Find("_GM").GetComponent<GM>().GetPrizes(Random.Range(25,200), 1000);
            Instantiate(blood, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("Hit something.");
        if(col.gameObject.tag == "Finish")
        {
            Debug.Log("Dealing damage!");
            GameObject.Find("_GM").GetComponent<GM>().looseLife(1);
            Destroy(gameObject);
        }
    }
}
