This is a game made for the Ludum Dare 46. All of it's components were made by their creators within 72 hours from start to finish. The people who made this game are:
Maksymilian "Skill" Tym - Programming
Tymon "Cham3k" Hyc - SFX/Music
Maja "海月" Kępczyńska - Graphics
Kaja "Mango" Podżorska - Graphics
Julia "Sok" Czyżewska - Graphics